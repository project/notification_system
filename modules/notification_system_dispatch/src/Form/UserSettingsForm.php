<?php

namespace Drupal\notification_system_dispatch\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\notification_system_dispatch\NotificationSystemDispatcherInterface;
use Drupal\notification_system_dispatch\NotificationSystemDispatcherPluginManager;
use Drupal\notification_system_dispatch\Service\UserSettingsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Notification System Dispatch form.
 */
class UserSettingsForm extends FormBase {

  /**
   * The user setting service.
   *
   * @var \Drupal\notification_system_dispatch\Service\UserSettingsService
   */
  protected $userService;

  /**
   * The dispatch plugin manager.
   *
   * @var \Drupal\notification_system_dispatch\NotificationSystemDispatcherPluginManager
   */
  protected $dispatcherPluginManager;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('notification_system_dispatch.user_settings'),
      $container->get('plugin.manager.notification_system_dispatcher'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new user settings form.
   *
   * @param \Drupal\notification_system_dispatch\Service\UserSettingsService $user_service
   *   The user setting service.
   * @param \Drupal\notification_system_dispatch\NotificationSystemDispatcherPluginManager $dispatcher_plugin_manager
   *   The dispatch plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Manager.
   */
  public function __construct(UserSettingsService $user_service, NotificationSystemDispatcherPluginManager $dispatcher_plugin_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->userService = $user_service;
    $this->dispatcherPluginManager = $dispatcher_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'notification_system_dispatch_usersettings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pluginDefinitions = $this->dispatcherPluginManager->getDefinitions();
    $groups = $this->entityTypeManager->getStorage('notification_group')->loadMultiple();
    $group_settings = $this->userService->dispatcherGroupEnabled();

    foreach ($pluginDefinitions as $definition) {
      /** @var \Drupal\notification_system_dispatch\NotificationSystemDispatcherInterface $dispatcher */
      $dispatcher = $this->dispatcherPluginManager->createInstance($definition['id']);
      $id = $dispatcher->id();

      $form['container-' . $id] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['dispatcher-group'],
        ],
      ];

      $form['container-' . $id]['dispatcher_' . $id] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Receive notifications via @dispatcher', [
          '@dispatcher' => $this->t($dispatcher->label(), [], [
            'context' => 'notification_system dispatcher label',
          ]),
        ]),
        '#default_value' => $this->userService->dispatcherEnabled($id),
        '#ajax' => [
          'callback' => '::autosave',
          'event' => 'change',
          'wrapper' => 'ajax_placeholder',
          'progress' => [
            'type' => 'throbber',
            'message' => '',
          ],
        ],
      ];

      // Setting per dispatcher, per group.
      foreach ($groups as $gid => $group) {
        $form['container-' . $id]['dispatcher_' . $id . '_group_' . $gid] = [
          '#type' => 'checkbox',
          '#title' => $group->label(),
          '#default_value' => $group_settings[$id][$gid] ?? TRUE,
          '#ajax' => [
            'callback' => '::autosave',
            'event' => 'change',
            'wrapper' => 'ajax_placeholder',
            'progress' => [
              'type' => 'throbber',
              'message' => '',
            ],
          ],
          '#wrapper_attributes' => [
            'class' => ['group-item'],
          ],
          '#states' => [
            'disabled' => [
              ':input[name="dispatcher_' . $id . '"]' => ['checked' => FALSE],
            ],
          ],
        ];
      }
    }

    $enableBundling = $this->config('notification_system_dispatch.settings')->get('enable_bundling');
    if ($enableBundling) {
      $form['send_mode'] = [
        '#type' => 'select',
        '#title' => $this->t('When do you want to receive the notifications?'),
        '#options' => [
          NotificationSystemDispatcherInterface::SEND_MODE_IMMEDIATELY => $this->t('Immediately'),
          NotificationSystemDispatcherInterface::SEND_MODE_DAILY => $this->t('Daily summary'),
          NotificationSystemDispatcherInterface::SEND_MODE_WEEKLY => $this->t('Weekly summary'),
        ],
        '#default_value' => $this->userService->getSendMode(),
        '#ajax' => [
          'callback' => '::autosave',
          'event' => 'change',
          'wrapper' => 'ajax_placeholder',
          'progress' => [
            'type' => 'throbber',
            'message' => '',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * Autosave callback for the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return string[]
   *   The save message markup.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function autosave(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();

    // We're dealing with dispatch settings.
    if (strpos($triggering_element['#name'], 'dispatcher_') === 0) {
      $trigger = str_replace('dispatcher_', '', $triggering_element['#name']);

      // A group setting.
      if ($pluginId = strstr($trigger, '_group_', TRUE)) {
        $group_id = str_replace($pluginId . '_group_', '', $trigger);
        $this->userService->setDispatcherGroupEnabled($pluginId, $group_id, $triggering_element['#value']);
      }
      // Main Dispatcher checkbox.
      else {
        $this->userService->setDispatcherEnabled($trigger, $triggering_element['#value']);
      }
    }

    if ($triggering_element['#name'] === 'send_mode') {
      $enableBundling = $this->config('notification_system_dispatch.settings')->get('enable_bundling');

      if ($enableBundling) {
        $sendMode = $triggering_element['#value'];
        $this->userService->setSendMode($sendMode);
      }
    }

    return [
      '#markup' => '<div class="hidden">Saved</div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
