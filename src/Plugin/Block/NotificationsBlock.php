<?php

namespace Drupal\notification_system\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Provides a notifications block.
 *
 * @Block(
 *   id = "notification_system_notifications",
 *   admin_label = @Translation("Notifications"),
 *   category = @Translation("Notification System")
 * )
 */
class NotificationsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The router.
   *
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $router;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('router.no_access_checks'),
      $container->get('class_resolver')
    );
  }

  /**
   * NotificationsBlock constructor.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Symfony\Component\Routing\RouterInterface $router
   *   The router.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RouterInterface $router, ClassResolverInterface $classResolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->router = $router;
    $this->classResolver = $classResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_mode' => 'simple',
      'show_read' => FALSE,
      'hide_empty' => FALSE,
      'empty_message' => '',
      'disable_ajax' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['display_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Display Mode'),
      '#options' => [
        'simple' => $this->t('Simple (No bundling)'),
        'bundled' => $this->t('Bundled by notification group'),
      ],
      '#default_value' => $this->configuration['display_mode'] ?: 'simple',
    ];

    $form['show_read'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show read notifications'),
      '#description' => $this->t('Read notifications will be shown below the unread notifications'),
      '#default_value' => $this->configuration['show_read'],
    ];

    $form['hide_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide emtpy notifications'),
      '#description' => $this->t('Hides the dropdown if there are no notifications'),
      '#default_value' => $this->configuration['hide_empty'],
    ];

    $form['empty_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Empty notifications message.'),
      '#description' => $this->t('Displays this message if not hidden and no notifications exist.'),
      '#default_value' => $this->configuration['empty_message'],
    ];

    $form['disable_ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable AJAX'),
      '#description' => $this->t('Build this block at runtime instead of AJAX.'),
      '#default_value' => $this->configuration['disable_ajax'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display_mode'] = $form_state->getValue('display_mode');
    $this->configuration['show_read'] = $form_state->getValue('show_read');
    $this->configuration['hide_empty'] = $form_state->getValue('hide_empty');
    $this->configuration['empty_message'] = $form_state->getValue('empty_message');
    $this->configuration['disable_ajax'] = $form_state->getValue('disable_ajax');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // Only show the block for logged-in users.
    return AccessResult::allowedIf($account->isAuthenticated());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $loading = $this->t('Loading');
    if ($this->configuration['disable_ajax']) {
      // Use the router to allow overrides.
      $controllerDefault = $this->router->getRouteCollection()
        ->get('notification_system.getnotifications')
        ->getDefaults()['_controller'];
      [$controllerClass] = explode('::', $controllerDefault);
      $notificationController = $this->classResolver->getInstanceFromDefinition($controllerClass);
      $content = $notificationController->buildRenderableNotifications($this->configuration['display_mode'], $this->configuration['show_read']);
      // Add in caches for auto-placeholdered content.
      $content['#cache']['contexts'][] = 'user';
      $content['#cache']['tags'][] = 'notification_system:read:' . $this->currentUser->id();
    }
    else {
      $content = ['#markup' => '<p>' . $loading . '...</p>'];
    }

    $build['content'] = [
      '#theme' => 'notification_block',
      '#display_mode' => $this->configuration['display_mode'],
      '#show_read' => $this->configuration['show_read'],
      '#hide_empty' => $this->configuration['hide_empty'],
      '#empty_message' => $this->configuration['empty_message'],
      '#disable_ajax' => $this->configuration['disable_ajax'],
      '#content' => $content,
      '#attached' => [
        'library' => [
          'notification_system/notifications_block',
        ],
        'drupalSettings' => [
          'notificationSystem' => [
            'getEndpointUrl' => Url::fromRoute('notification_system.getnotifications', ['display_mode' => 'DISPLAY_MODE'])
              ->toString(),
            'markAsReadEndpointUrl' => Url::fromRoute('notification_system.markasread', [
              'providerId' => 'PROVIDER_ID',
              'notificationId' => 'NOTIFICATION_ID',
            ])->toString(),
          ],
        ],
      ],
    ];

    if (!$this->configuration['disable_ajax'] && $this->configuration['display_mode'] == 'bundled') {
      $build['content']['#content'] = [];

      $groupManager = $this->entityTypeManager->getStorage('notification_group');

      /** @var \Drupal\notification_system\Entity\NotificationGroupInterface[] $groups */
      $groups = $groupManager->loadMultiple();

      foreach ($groups as $group) {
        $build['content']['#content'][$group->id()] = [
          '#theme' => 'notification_group',
          '#group' => $group,
          '#id' => $group->id(),
          '#label' => $group->label(),
          '#description' => [
            '#type' => 'processed_text',
            '#text' => $group->getDescription()['value'],
            '#format' => $group->getDescription()['format'],
          ],
          '#content' => [
            '#markup' => '<p>' . $loading . '...</p>',
          ],
        ];
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Tag cache by marked read if no ajax..
    if ($this->configuration['disable_ajax']) {
      return Cache::mergeTags(parent::getCacheTags(), ['notification_system:read:' . $this->currentUser->id()]);
    }
    return parent::getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Vary caching of this block per user if not using ajax.
    if ($this->configuration['disable_ajax']) {
      return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
    }
    return parent::getCacheContexts();
  }

}
